import React, { Component } from 'react';
import {FlatList, StyleSheet, View} from "react-native";
import {connect} from "react-redux";
import {getData} from "../store/actions";
import PostItem from "./PostItem";

class Posts extends Component {

  componentDidMount() {
    this.props.getPosts();
  }

  loadMoreHandler = () => {
    this.props.getPosts(this.props.after);
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          keyExtractor={item => item.id}
          style={styles.postList}
          data={this.props.posts}
          renderItem={(info) => (
            <PostItem
              image={info.item.thumbnail}
              title={info.item.title}
            />
          )}
          onEndReached={this.loadMoreHandler}
          onEndReachedThreshold={0.5}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10
  },
  postList: {
    width: '100%'
  }
});

mapStateToProps = state => {
  return {
    posts: state.posts,
    after: state.after
  };
};

mapDispatchToProps = dispatch => {
  return {
    getPosts: () => dispatch(getData())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Posts);