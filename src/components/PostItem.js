import React from 'react';
import {Text, View, StyleSheet, Image, TouchableNativeFeedback} from "react-native";

const PostItem = (props) => {
  return(
    <TouchableNativeFeedback key={props.id}>
      <View style={styles.postItem} >
        <Image resizeMode='contain' source={{uri: props.image}} style={styles.image} />
        <Text style={styles.text}>{props.title}</Text>
      </View>
    </TouchableNativeFeedback>
  )
};

const styles = StyleSheet.create({
  postItem: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#eee',
    padding: 10,
    marginBottom: 10,
    paddingRight: 65,
    width: '100%'
  },
  image: {
    width: 50,
    height: 50,
    marginRight: 10
  },
  text: {
    fontSize: 10,
    marginRight: 10
  }
});

export default PostItem;