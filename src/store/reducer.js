import {FETCH_POST_FAILURE, FETCH_POST_REQUEST, FETCH_POST_SUCCESS} from "./actions";

const initialState = {
  posts: [],
  after: '',
  loading: false,
  error: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POST_REQUEST:
      return {...state, loading: true};
    case FETCH_POST_SUCCESS:
      return {...state, posts: state.posts.concat(action.data), after: action.after};
    case FETCH_POST_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;