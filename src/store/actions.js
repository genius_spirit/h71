import axios from 'axios';

export const FETCH_POST_REQUEST = "FETCH_POST_REQUEST";
export const FETCH_POST_SUCCESS = "FETCH_POST_SUCCESS";
export const FETCH_POST_FAILURE = "FETCH_POST_FAILURE";

const url = 'https://www.reddit.com/r/pics.json';

export const getData = (after) => {
  return dispatch => {
    dispatch(fetchPostRequest());
    axios.get(`${url}?count=25&after=${after}`)
    .then(response => {
      const result = response.data;
      const children = result.data.children;
      const after = result.data.after;
      const data = children.map(item => {
        return {id: item.data.id, thumbnail: item.data.thumbnail, title: item.data.title}
      });
      dispatch(fetchPostSuccess(data, after));
    }, error => {
      dispatch(fetchPostFailure(error));
    });
  }
};

const fetchPostRequest = () => {
  return {type: FETCH_POST_REQUEST}
};

const fetchPostSuccess = (data, after) => {
  return {type: FETCH_POST_SUCCESS, data, after: after};
};

const fetchPostFailure = (error) => {
  return {type: FETCH_POST_FAILURE, error}
};