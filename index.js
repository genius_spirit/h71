import React from 'react';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

import reducer from './src/store/reducer';
import App from "./App";
import { AppRegistry } from "react-native";

const store = createStore(reducer, applyMiddleware(thunk));

const Application = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent('Reddit', () => Application);
